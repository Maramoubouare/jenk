<!DOCTYPE html>
<html lang="en">
<head>
		<?php 
	session_start(); 
 	include('file:///C|/wamp/www/connexion/cn.php'); 
        ?>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Logisti ">
  <link href="file:///C|/wamp/www/assets/images/favicon/favicon.png" rel="icon">
  <!-- TemplateBeginEditable name="doctitle" -->
  <title>Logisti</title>
  <!-- TemplateEndEditable -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:400,700%7cWork+Sans:400,600,700&display=swap">
  <link rel="stylesheet" href="file:///C|/wamp/www/assets/css/libraries.css" />
  <link rel="stylesheet" href="file:///C|/wamp/www/assets/css/style.css" />
  <!-- TemplateBeginEditable name="head" -->
  <!-- TemplateEndEditable -->
</head>
<body>
  <div class="wrapper">
    <!-- =========================
        Header
    =========================== -->
    <header id="header" class="header header-transparent">
      <nav class="navbar navbar-expand-lg" style="background-color: #a26868">
        <div class="container">
          <a class="navbar-brand" href="file:///C|/wamp/www/index.html">
            <img src="file:///C|/wamp/www/assets/images/logo/logo-light.png" class="logo-light" alt="logo">
            <img src="file:///C|/wamp/www/assets/images/logo/logo-dark.png" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>
        <!-- /.header-top-right -->
			<div class="header__top-right d-none d-lg-block">
            <ul class="list-unstyled d-flex justify-content-end align-items-center">
              <li><i class="icon-phone"></i><span>+55 654 541 17</span></li>
              <li>
                <div class="dropdown">
                  <button class="btn dropdown-toggle width-auto" id="langDrobdown" data-toggle="dropdown">
                    <i class="icon-map"></i><span>En</span>
                  </button>
                </div>
              </li>
              <li><a href="../devis.php" class="btn btn__white"><i class="icon-list"></i><span>demande de devis</span></a></li>
            </ul>
          </div>
			
          <div class="collapse navbar-collapse" id="mainNavigation">
            <ul class="navbar-nav ml-auto">
              <li class="nav__item with-dropdown">
                <a href="file:///C|/wamp/www/index.php" class="dropdown-toggle nav__item-link active">Acceuil</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                 
              </li><!-- /.nav-item -->
				              <li class="nav__item with-dropdown">
                <a href="file:///C|/wamp/www/presentation.php" class="dropdown-toggle nav__item-link ">presentation</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                 
              </li>
              <li class="nav__item with-dropdown">
                <a  href="#" class="dropdown-toggle nav__item-link">types de conteneur</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                <ul class="dropdown-menu">
                  <li class="nav__item"><a href="file:///C|/wamp/www/conteneurs_std.php" class="nav__item-link">standars</a></li>
                  <!-- /.nav-item -->
                  <li class="nav__item"><a href="file:///C|/wamp/www/conteneurs_fig.php" class="nav__item-link">frigorifirique</a></li>
					 <li class="nav__item"><a href="file:///C|/wamp/www/conteneurs_spec.php" class="nav__item-link">specifique</a></li>
               
                </ul><!-- /.dropdown-menu -->
              </li><!-- /.nav-item -->
  
				
				<li class="nav__item with-dropdown ">
                <a href="#" class="dropdown-toggle nav__item-link">Services</a>
                <i class="fa fa-angle-right" data-toggle="dropdown" aria-expanded="true"></i>
                <ul class="dropdown-menu ">
                  <li class="nav__item"><a href="file:///C|/wamp/www/transformatin.php" class="nav__item-link">Amenagement et transformation</a></li>
                  <!-- /.nav-item -->
                  <li class="nav__item"><a href="file:///C|/wamp/www/transport.php" class="nav__item-link">Transport</a></li>
					 <li class="nav__item"><a href="file:///C|/wamp/www/location.php" class="nav__item-link">Location</a></li>
                  <!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->
              </li>
             
             
      
              
              <li class="nav__item">
                <a href="file:///C|/wamp/www/contacs.php" class="nav__item-link">Contact </a>
              </li><!-- /.nav-item -->
            </ul><!-- /.navbar-nav -->
          </div><!-- /.navbar-collapse -->

        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->
	  
	  
	  
	  
	  
	  
	  
	  

    <footer id="footer" class="footer footer-1">
      <!-- /.footer-top -->
      <div class="container">
        <div class="footer-bottom">
          <div class="row">
            <div class="col-sm-12 col-md-9 col-lg-9">
              <div class="footer__copyright">
                <nav>
                  <ul class="list-unstyled footer__copyright-links d-flex flex-wrap">
                    <li><a href="../mention.php">Mentions légales </a></li>
                  </ul>
                </nav>
                <span>&copy; 2020 </span>
                
              </div><!-- /.Footer-copyright -->
            </div><!-- /.col-lg-6 -->
            <div class="col-sm-12 col-md-3 col-lg-3 d-flex align-items-center">
              <div class="social__icons justify-content-end w-100">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
              </div><!-- /.social-icons -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
        </div><!-- /.Footer-bottom -->
      </div><!-- /.container -->
    </footer><!-- /.Footer -->
    <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>

    <div class="module__search-container">
      <i class="fa fa-times close-search"></i>
      <form class="module__search-form">
        <input type="text" class="search__input" placeholder="Search Here">
        <button class="module__search-btn"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- /.module-search-container -->

  </div><!-- /.wrapper -->

  <script src="file:///C|/wamp/www/assets/js/jquery-3.3.1.min.js"></script>
  <script src="file:///C|/wamp/www/assets/js/plugins.js"></script>
  <script src="file:///C|/wamp/www/assets/js/main.js"></script>
</body>

</html>